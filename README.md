This program converts a .png image into ANSI control sequences for
a Linux terminal that supports 24-bit ANSI colors.

Usage: img2ansi \<filename\>  
At the moment only PNG-files are supported.

Warning: This is an early work-in-progress.  
Only tested with Ubuntu 19.10 and rustc 1.42.0 on amd64 so far.

### Installation

- Install a current version of Rust, e.g. using [rustup](https://rustup.rs/).
- `cargo install --git 'https://gitlab.com/sora1248/img2ansi'`
- You should be able to use it now (see above).
