use lodepng::{Bitmap, RGB};
use std::cmp::min;
use std::collections::HashMap;
use std::env;
use std::error::Error;
use std::hash::Hash;
use std::io::Write;
use std::mem::swap;

#[derive(Clone, Eq, PartialEq, Hash)]
pub struct AnsiBitmap {
    fg: Option<RGB<u8>>,
    bg: Option<RGB<u8>>,
    inv: bool,
}

#[derive(Copy, Clone)]
enum ColorPos {
    None,
    Foreground,
    Background,
}

pub trait IAnsiBitmap: Clone {
    fn put_pixel(
        &mut self,
        output: &mut dyn std::io::Write,
        top: &RGB<u8>,
        bottom: Option<&RGB<u8>>,
    ) -> std::io::Result<()>;
    fn newline(&mut self, output: &mut dyn std::io::Write) -> std::io::Result<()>;
    fn finish(&mut self, output: &mut dyn std::io::Write) -> std::io::Result<()>;

    fn ansify(
        &mut self,
        output: &mut dyn std::io::Write,
        bitmap: Bitmap<RGB<u8>>,
    ) -> std::io::Result<()> {
        for rows in bitmap.buffer.chunks(2 * bitmap.width) {
            let (top, bottom) = rows.split_at(bitmap.width);
            if !bottom.is_empty() {
                for (px1, px2) in top.into_iter().zip(bottom) {
                    self.put_pixel(output, px1, Some(px2))?;
                }
            } else {
                for px in top {
                    self.put_pixel(output, px, None)?;
                }
            }
            self.newline(output)?;
        }
        self.finish(output)?;
        Ok(())
    }
}

impl AnsiBitmap {
    pub fn new() -> Self {
        AnsiBitmap {
            fg: None,
            bg: None,
            inv: false,
        }
    }

    fn get_xterm256(color: &RGB<u8>) -> Option<u8> {
        // Colors 0..15 won't be used as those are commonly changed to look nicer.
        let quantize = |v| match v {
            0 => Some(0u8),
            v if (v >= 95) && ((v - 95) % 40 == 0) => Some((v - 55) / 40),
            _ => None,
        };
        let r = quantize(color.r);
        let g = quantize(color.g);
        let b = quantize(color.b);
        if let (Some(r), Some(g), Some(b)) = (r, g, b) {
            return Some(16 + 36 * r + 6 * g + b);
        }
        if (color.r == color.g) && (color.r == color.b) {
            if (color.r >= 8) && (color.r <= 238) && ((color.r - 8) % 10 == 0) {
                return Some(232 + (color.r - 8) / 10);
            }
        }
        None
    }

    fn get_fg(&self) -> Option<RGB<u8>> {
        if self.inv {
            self.bg
        } else {
            self.fg
        }
    }

    fn get_bg(&self) -> Option<RGB<u8>> {
        if self.inv {
            self.fg
        } else {
            self.bg
        }
    }

    pub fn gen_ansi_color(
        &mut self,
        output: &mut dyn std::io::Write,
        color: &RGB<u8>,
        background: bool,
    ) -> std::io::Result<()> {
        let (current, code) = if background ^ self.inv {
            (&mut self.bg, 48)
        } else {
            (&mut self.fg, 38)
        };
        if *current == Some(*color) {
            return Ok(());
        }
        *current = Some(*color);
        match AnsiBitmap::get_xterm256(color) {
            Some(idx) => write!(output, "\x1b[{};5;{}m", code, idx)?,
            None => write!(
                output,
                "\x1b[{};2;{};{};{}m",
                code, color.r, color.g, color.b
            )?,
        }
        Ok(())
    }

    fn find_color(&self, color: &RGB<u8>) -> ColorPos {
        if self.get_bg() == Some(*color) {
            ColorPos::Background
        } else if self.get_fg() == Some(*color) {
            ColorPos::Foreground
        } else {
            ColorPos::None
        }
    }

    pub fn set_inv(&mut self, output: &mut dyn std::io::Write, inv: bool) -> std::io::Result<()> {
        if inv != self.inv {
            self.inv = inv;
            write!(output, "\x1b[{}m", if inv { 7 } else { 27 })?;
        }
        Ok(())
    }
}

impl IAnsiBitmap for AnsiBitmap {
    fn put_pixel(
        &mut self,
        output: &mut dyn Write,
        top: &RGB<u8>,
        bottom: Option<&RGB<u8>>,
    ) -> std::io::Result<()> {
        match (top, bottom) {
            (t, None) => {
                self.gen_ansi_color(output, t, false)?;
                write!(output, "▀")?;
            }
            (t, Some(b)) if t == b => match self.find_color(t) {
                ColorPos::None => {
                    self.gen_ansi_color(output, b, true)?;
                    write!(output, " ")?;
                }
                ColorPos::Background => write!(output, " ")?,
                ColorPos::Foreground => write!(output, "█")?,
            },
            (t, Some(b)) => {
                let mut tp = self.find_color(t);
                let mut bp = self.find_color(b);
                match (tp, bp) {
                    (ColorPos::None, ColorPos::Background) => {
                        self.gen_ansi_color(output, t, false)?;
                        tp = ColorPos::Foreground;
                    }
                    (ColorPos::None, ColorPos::Foreground) => {
                        self.gen_ansi_color(output, t, true)?;
                        tp = ColorPos::Background;
                    }
                    (ColorPos::Background, ColorPos::None) => {
                        self.gen_ansi_color(output, b, false)?;
                        bp = ColorPos::Foreground;
                    }
                    (ColorPos::Foreground, ColorPos::None) => {
                        self.gen_ansi_color(output, b, true)?;
                        bp = ColorPos::Background;
                    }
                    (ColorPos::None, ColorPos::None) => {
                        self.gen_ansi_color(output, t, false)?;
                        self.gen_ansi_color(output, b, true)?;
                        tp = ColorPos::Foreground;
                        bp = ColorPos::Background;
                    }
                    _ => (),
                }
                let code = match (tp, bp) {
                    (ColorPos::Background, ColorPos::Background) => ' ',
                    (ColorPos::Background, ColorPos::Foreground) => '▄',
                    (ColorPos::Foreground, ColorPos::Background) => '▀',
                    (ColorPos::Foreground, ColorPos::Foreground) => '█',
                    _ => unreachable!(),
                };
                write!(output, "{}", code)?;
            }
        }
        Ok(())
    }

    fn newline(&mut self, output: &mut dyn Write) -> std::io::Result<()> {
        self.set_inv(output, false)?;
        if self.bg != None {
            write!(output, "\x1b[49m")?;
            self.bg = None;
        }
        writeln!(output)?;
        Ok(())
    }

    fn finish(&mut self, output: &mut dyn Write) -> std::io::Result<()> {
        if self.fg != None || self.bg != None {
            write!(output, "\x1b[0m")?;
            self.fg = None;
            self.bg = None;
        }
        Ok(())
    }
}

#[derive(Clone)]
pub struct DynProgAnsiBitmap {
    states: HashMap<AnsiBitmap, Vec<u8>>,
}

impl DynProgAnsiBitmap {
    pub fn new() -> Self {
        let mut states = HashMap::new();
        states.insert(AnsiBitmap::new(), Vec::new());
        DynProgAnsiBitmap { states }
    }

    fn update_state(&mut self, k: AnsiBitmap, v: Vec<u8>) {
        use std::collections::hash_map::Entry::*;
        match self.states.entry(k) {
            Vacant(e) => {
                e.insert(v);
            }
            Occupied(mut e) => {
                if e.get().len() > v.len() {
                    e.insert(v);
                }
            }
        }
    }

    fn compare(v1: &[u8], v2: &[u8]) -> usize {
        let max_len = min(v1.len(), v2.len());
        for (idx, (&c1, &c2)) in v1.iter().zip(v2).enumerate() {
            if c1 != c2 {
                return idx;
            }
        }
        max_len
    }

    fn drain(&mut self, output: &mut dyn std::io::Write) -> std::io::Result<()> {
        assert!(!self.states.is_empty());
        let mut iter = self.states.iter();
        let ref_vec = iter.next().unwrap().1;
        let mut len = ref_vec.len();
        while let Some((_, vec)) = iter.next() {
            len = DynProgAnsiBitmap::compare(&ref_vec[0..len], &vec);
            if len == 0 {
                break;
            }
        }
        if len > 0 {
            output.write(&ref_vec[0..len])?;
            for (_, v) in self.states.iter_mut() {
                let mut idx = 0;
                v.retain(|_| {
                    idx += 1;
                    idx > len
                });
            }
        }
        Ok(())
    }
}

impl IAnsiBitmap for DynProgAnsiBitmap {
    fn put_pixel(
        &mut self,
        output: &mut dyn Write,
        top: &RGB<u8>,
        bottom: Option<&RGB<u8>>,
    ) -> std::io::Result<()> {
        let mut old_states = HashMap::new();
        swap(&mut old_states, &mut self.states);
        if let Some(btm) = bottom {
            for (k, v) in old_states.drain() {
                for flipped in &[false, true] {
                    for inv in &[false, true] {
                        let mut k = k.to_owned();
                        let mut v = v.to_owned();
                        k.set_inv(&mut v, *inv)?;
                        k.gen_ansi_color(&mut v, top, *flipped)?;
                        if top != btm {
                            k.gen_ansi_color(&mut v, btm, !*flipped)?;
                        }
                        k.put_pixel(&mut v, top, bottom)?;
                        self.update_state(k, v);
                    }
                }
            }
        } else {
            for (k, v) in old_states.drain() {
                let mut k = k;
                let mut v = v;
                k.put_pixel(&mut v, top, bottom)?;
                self.update_state(k, v);
            }
        }
        self.drain(output)
    }

    fn newline(&mut self, output: &mut dyn Write) -> std::io::Result<()> {
        let mut old_states = HashMap::new();
        swap(&mut old_states, &mut self.states);
        for (k, v) in old_states.drain() {
            let mut k = k;
            let mut v = v;
            k.newline(&mut v)?;
            self.update_state(k, v);
        }
        self.drain(output)
    }

    fn finish(&mut self, output: &mut dyn Write) -> std::io::Result<()> {
        let mut best = Vec::new();
        for (_, v) in self.states.drain() {
            if best.is_empty() || v.len() < best.len() {
                best = v;
            }
        }

        output.write(&best)?;

        self.states.insert(AnsiBitmap::new(), Vec::new());
        Ok(())
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<_> = env::args().collect();
    if args.len() == 1 {
        println!("Usage: {} <image-filename.png>", args[0]);
        return Ok(());
    }
    let bitmap = lodepng::decode24_file(&args[1])?;
    DynProgAnsiBitmap::new().ansify(&mut std::io::stdout(), bitmap)?;
    Ok(())
}
